# Komputer Store
## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install
- Install Visual Studio Code
- Install Live Server extension
- Clone repository
- Run index.html with liveserver
## Usage
- A simple banking and store system, that allows users to work and earn money. Deposit earnings in bank and get a loan. In addition the user can buy laptopts retrived from laptop API
## Maintainers
[@magnussmeby](https://gitlab.com/magnussmeby)
## Contributing
PRs accepted.
Small note: If editing the README, please conform to the standard-readme specification.
## License
MIT © 2021 Noroff Accelerate
