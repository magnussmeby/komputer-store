const bankBalance = document.getElementById("banker-balance");
const workBalance = document.getElementById("work-balance");
const description = document.getElementById("description");
const loanButton = document.getElementById("loan-button");
const bankButton = document.getElementById("bank-button");
const workButton = document.getElementById("work-button");
const buyButton = document.getElementById("buy-button");
const features = document.getElementById("features");
const dropDown = document.getElementById("dropdown");
const loanBalance = document.getElementById("loan");
const payLoan = document.getElementById("pay-loan");
const image = document.getElementById("image");
const model = document.getElementById("model");
const price = document.getElementById("price");

let computers = [];
let totalBalance = 0;
let wBalance = 0;
let loan = 0;
let pay = 0;

bankBalance.innerHTML = "Balance " + totalBalance + " kr";
workBalance.innerHTML = "Pay " + wBalance + " kr";

fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => {
        addComputers(computers)
        model.innerHTML = computers[0].title;
        image.src = 'https://noroff-komputer-store-api.herokuapp.com/' + computers[0].image;
        price.value = computers[0].price
        price.innerHTML = computers[0].price + " NOK"
        description.innerHTML = computers[0].description
        computers[0].specs.forEach((item) => {
            let li = document.createElement("li");
            li.innerHTML = item;
            features.appendChild(li);
        })
    });

const addComputers = (computers) => {
    computers.forEach(x => addComputer(x))
}
const addComputer = (computer) => {
    let comp = document.createElement("option");
    comp.value = computer.id;
    comp.innerHTML = computer.title;
    dropDown.appendChild(comp);
}

const work = () => {
    wBalance += 100;
    setBalance(workBalance, wBalance, "Pay");
}

const payNow = () => {
    if (wBalance > loan) {
        payLoan.style.visibility = "hidden";
        loanBalance.innerHTML = "";
        wBalance -= loan;
        totalBalance += wBalance;
        loan = 0;
        wBalance = 0;
        setBalance(workBalance, wBalance, "Pay");
        setBalance(bankBalance, totalBalance, "Balance");
    } else {
        loan -= wBalance;
        wBalance = 0;
        setBalance(loanBalance, loan, "Loan");
        setBalance(workBalance, wBalance, "Pay");
    }
}

const transfer = () => {
    if (loan != 0) {
        loan -= (wBalance * 0.1);
        setBalance(loanBalance, loan, "Loan");
        totalBalance += wBalance * 0.90;
        if (loan < 0) {
            totalBalance += loan * -1;
            loan = 0;
        }
        setBalance(bankBalance, totalBalance, "Balance");
        loanBalance.innerHTML = "";
        wBalance = 0;
        setBalance(workBalance, wBalance, "Pay");
    } else {
        totalBalance += wBalance;
        wBalance = 0;
        setBalance(bankBalance, totalBalance, "Balance");
        setBalance(workBalance, wBalance, "Pay");
    }

}

const getLoan = () => {
    if (loan > 0) {
        window.alert("You can only have one loan at the time");
    } else {
        let amount = window.prompt("Fill in loan amount");
        if (!isNaN(amount) && amount.length != 0) {
            let intAmount = parseInt(amount);
            if (intAmount > totalBalance * 2) {
                window.alert("Loan amount is to high")
            } else {
                loan = intAmount;
                totalBalance += loan;
                setBalance(loanBalance, loan, "Loan");
                setBalance(bankBalance, totalBalance, "Balance")
                payLoan.style.visibility = "visible";
            }
        } else {
            window.alert("Invalid input");
        }
    }
}

const displayComputer = e => {
    const selected = computers[e.target.selectedIndex];
    description.innerHTML = selected.description;
    model.innerHTML = selected.title;
    price.innerHTML = selected.price + " NOK";
    price.value = selected.price;
    image.src = 'https://noroff-komputer-store-api.herokuapp.com/' + selected.image;
    features.innerHTML = "";
    selected.specs.forEach((item) => {
        let li = document.createElement("li");
        li.innerHTML = item;
        features.appendChild(li);
    })
}

const setBalance = (element, newBalance, type) => {
    element.innerHTML = type + " " + newBalance + " kr";
}

const buyLaptop = () => {
    if (price.value > totalBalance) {
        window.alert("You dont have enough money in the bank")
    } else {
        totalBalance -= price.value;
        setBalance(bankBalance, totalBalance, "Balance");
        window.alert("You are now the proud owner of a new computer");
    }
}

workButton.addEventListener("click", work);
loanButton.addEventListener("click", getLoan);
bankButton.addEventListener("click", transfer);
payLoan.addEventListener("click", payNow);
dropDown.addEventListener("change", displayComputer);
buyButton.addEventListener("click", buyLaptop);